/**
 * @File Name          : ClausesAutomationTriggerHelper.cls
 * @Description        : This is a helper class for ClausesAutomationTrigger.trigger which updates the related Contract Vehicle record
                        when Contract Clauses is inserted/deleted for any of these clauses(52.227-11, 52.227-13, 252.227-7038 or 252.227-7039)
 * @Author             : Shyam Nasare
 * @Last Modified By   : Shyam Nasare
 * @Last Modified On   : 1/6/2020, 5:04:50 PM

 * Ver       Date            Author      		    Modification
 * 1.0    1/6/2020         Shyam Nasare            Initial Version
**/
public with sharing class ClausesAutomationTriggerHelper {

    public static void updateContractVehicle(List<TM_GovSuite__Contract_Clauses__c> contractClauses){
        Set<Id> contractVehicleIds = new Set<Id>();
        List<TM_GovSuite__Contract_Vehicle__c> contractList = new List<TM_GovSuite__Contract_Vehicle__c>();
        List<TM_GovSuite__Contract_Vehicle__c> contractUpdateList = new List<TM_GovSuite__Contract_Vehicle__c>();
        Set<String> clausesName = new Set<String>();
        
        for(ClausesName__c cm : ClausesName__c.getAll().values()){
            clausesName.add(cm.Clause__c);
        }

        System.debug('clausesName --> '+clausesName);

        for(TM_GovSuite__Contract_Clauses__c clause : contractClauses){
            contractVehicleIds.add(clause.TM_GovSuite__Contract_Vehicle__c);
        }

        if(!contractVehicleIds.isEmpty()){
            contractList = [Select Id, Name, (Select Id, Clause_Name__c From TM_GovSuite__Contract_Clauses__r) 
                            From TM_GovSuite__Contract_Vehicle__c Where Id In :contractVehicleIds];
        }

        if(!contractList.isEmpty() && !clausesName.isEmpty()){
            for(TM_GovSuite__Contract_Vehicle__c cv : contractList){
                System.debug('Contract Vehicle --> '+cv.Name);
                Boolean flag = false;

                for(TM_GovSuite__Contract_Clauses__c clause : cv.TM_GovSuite__Contract_Clauses__r){
                    System.debug('Clause --> '+clause.Clause_Name__c);
                    if(clausesName.contains(clause.Clause_Name__c)){
                        flag = true;
                        break;
                    }
                }

                TM_GovSuite__Contract_Vehicle__c cvNew = new TM_GovSuite__Contract_Vehicle__c(Id=cv.Id);

                if(flag){
                    cvNew.Inventory_Patent_Submitted__c = 'Yes';
                }else{
                    cvNew.Inventory_Patent_Submitted__c = 'No';
                }

                contractUpdateList.add(cvNew);
            }

            if(!contractUpdateList.isEmpty()){
                try{
                    update contractUpdateList;
                }catch(Exception ex){
                    System.debug(ex);
                }
            }
        }
    }
}
