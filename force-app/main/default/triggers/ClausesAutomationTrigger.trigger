/**
 * @File Name          : ClausesAutomationTrigger.trigger
 * @Description        : This trigger updates the related Contract Vehicle record
                        when Contract Clauses is inserted/deleted for any of these clauses(52.227-11, 52.227-13, 252.227-7038 or 252.227-7039)
 * @HelperClass        : ClausesAutomationTriggerHelper.cls                      
 * @Author             : Shyam Nasare
 * @Last Modified By   : Shyam Nasare
 * @Last Modified On   : 1/6/2020, 5:03:52 PM
 
 * Ver       Date            Author      		    Modification
 * 1.0    1/6/2020         Shyam Nasare            Initial Version
**/
trigger ClausesAutomationTrigger on TM_GovSuite__Contract_Clauses__c (after insert, after delete) {
    if(trigger.isInsert){
        ClausesAutomationTriggerHelper.updateContractVehicle(trigger.new);
    }
    if(trigger.isDelete){
        ClausesAutomationTriggerHelper.updateContractVehicle(trigger.old);
    }
}